package com.example.mortti.myapplication6;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;


/**
 * Created by Mortti on 19.9.2015.
 */
public class MessageSender implements Runnable {
    Socket senderS;
    PrintWriter msout;
    String msout1;

    public MessageSender(Socket ss, String viesti){
        this.senderS = ss;
        this.msout1 = viesti;

        try {
            msout = new PrintWriter(ss.getOutputStream(), true);
        }
        catch (IOException e){

        }

    }
    public void run(){
        msout.println(msout1);
        msout.flush();

    }

}
