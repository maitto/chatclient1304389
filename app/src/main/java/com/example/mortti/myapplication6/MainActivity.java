package com.example.mortti.myapplication6;

import android.app.Activity;
import android.os.Bundle;
import android.os.Message;
import android.os.Handler;

import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.support.*;



import java.net.Socket;
import java.util.logging.LogRecord;

public class MainActivity extends AppCompatActivity {
    private Handler uiHandler	=	new	Handler()	{


        public void handleMessage(Message msg)	{
            if	(msg.what	==	0){
                TextView result	=	(TextView)findViewById(R.id.laatikko);
                result.append((String)msg.obj + "\n");
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Socket bb = new Socket();
        final TextView ikkuna = (TextView) findViewById(R.id.laatikko);
        Button btnn = (Button) findViewById(R.id.nappi);
        final EditText edit = (EditText) findViewById(R.id.tekstikentta);


        setContentView(R.layout.activity_main);

        Log.e("4","4");
        MessageReceiver r1 = new MessageReceiver(bb, uiHandler);
        Thread t1 = new Thread(r1);
        Log.e("3","3");
        t1.start();


        btnn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String viesti;
                Log.e("1", "1");
                viesti = edit.getText().toString();
                Log.e("2", "2");
                MessageSender ms = new MessageSender(bb, viesti);
                Thread t = new Thread(ms);
                t.start();
                edit.setText("");
            }
        });


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
