package com.example.mortti.myapplication6;

import android.os.Message;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.Socket;
import android.os.Handler;



/**
 * Created by Mortti on 19.9.2015.
 */
public class MessageReceiver implements Runnable{
    BufferedReader b1 = null;
    Handler handler;


    Socket rs;
    InetSocketAddress osoite;
    public MessageReceiver(Socket b, Handler h){

        this.rs = b;
        this.osoite = new InetSocketAddress("10.112.213.199",100);
        handler = h;

    }

    @Override
    public void run() {
        try {
            this.rs.connect(osoite);
            b1 = new BufferedReader(new InputStreamReader(rs.getInputStream()));
            Log.e("Try","Try");
            while (true){
                String text = b1.readLine();
                while (text != null){
                    Message msg = handler.obtainMessage();
                    msg.obj = text;
                    msg.what = 0;

                    if(!text.equals("")){
                        handler.sendMessage(msg);
                    }
                    text = b1.readLine();
                }
                b1.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
            Log.e("Catch","catch");

        }
    }
}
